/*
	Von Arceo
	B210FT
	S30-Activity
*/

// No.2

db.fruits.aggregate([

		{
		$match: { onSale: true }
		},

		{$count: "fruitsOnSale"}
])


// No.3

db.fruits.aggregate([

		{
		$match: { stock: { $gte: 20 } }
		},

		{$count: "enoughStock"}
])


// No.4

db.fruits.aggregate([
		{
			$match: { onSale: true }
		},
		{
			$group: {
				_id: "$supplier_id",
				avg_price: { $avg: "$price" },
			}
		}
])

// No.5

db.fruits.aggregate([
		// {
		// 	$match: { onSale: true }
		// },
		{
			$group: {
				_id: "$supplier_id",
				max_price: { $max: "$price" },
			}
		},

		{
		$sort: { max_price: 1 }
		}

])

// No.6

db.fruits.aggregate([
		// {
		// 	$match: { onSale: true }
		// },
		{
			$group: {
				_id: "$supplier_id",
				min_price: { $min: "$price" },
			}
		},

		{
		$sort: { min_price: 1 }
		}

])
